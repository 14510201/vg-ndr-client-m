
TARGET = NDRClient

requirements:
	sudo apt-get install liblog4cpp5-dev

run:
	sudo ${PWD}/build/$(TARGET) -i enp2s0 -S 0x80001ff1 -s 112.97.0.38 -d 112.97.0.47 -r 192.168.44.67 -p 6379 -g eb9a02ab45feb421

debug:
	sudo gdb --args ${PWD}/build/$(TARGET) -i enp2s0 -S 0x80001ff1 -s 112.97.0.38 -d 112.97.0.47 -r 192.168.44.67 -p 6379 -g eb9a02ab45feb421