#ifndef ConfigController_hpp
#define ConfigController_hpp

#include "../services/UserService.hpp"

#include "oatpp/web/server/api/ApiController.hpp"
#include "oatpp/parser/json/mapping/ObjectMapper.hpp"
#include "oatpp/core/macro/codegen.hpp"
#include "../dto/ConfigDto.hpp"
#include "../utils/ConfigService.hpp"

#include OATPP_CODEGEN_BEGIN(ApiController) //<- Begin Codegen

/**
 * User REST controller.
 */
class ConfigController : public oatpp::web::server::api::ApiController {
public:
  ConfigController(OATPP_COMPONENT(std::shared_ptr<ObjectMapper>, objectMapper))
    : oatpp::web::server::api::ApiController(objectMapper)
  {
    m_configService = ConfigService::getInstance();
  }
private:
  ConfigService* m_configService; // Create config service.
public:

  static std::shared_ptr<ConfigController> createShared(
    OATPP_COMPONENT(std::shared_ptr<ObjectMapper>, objectMapper) // Inject objectMapper component here as default parameter
  ){
    return std::make_shared<ConfigController>(objectMapper);
  }
  
  ENDPOINT_INFO(getConfig) {
    info->summary = "Get NDR Configuration";

    info->addResponse<Object<ConfigDto>>(Status::CODE_200, "application/json");
    info->addResponse<Object<StatusDto>>(Status::CODE_404, "application/json");
    info->addResponse<Object<StatusDto>>(Status::CODE_500, "application/json");
  }
  ENDPOINT("GET", "config", getConfig)
  {
    return createDtoResponse(Status::CODE_200, m_configService->getConfigDto());
  }
  
};

#include OATPP_CODEGEN_END(ApiController) //<- End Codegen

#endif /* ConfigController_hpp */