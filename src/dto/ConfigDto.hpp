#ifndef ConfigDto_hpp
#define ConfigDto_hpp

#include "oatpp/core/macro/codegen.hpp"
#include "oatpp/core/Types.hpp"

#include OATPP_CODEGEN_BEGIN(DTO)

class ConfigDto : public oatpp::DTO {
  
  DTO_INIT(ConfigDto, DTO)
  DTO_FIELD(String, interface, "interface");
  DTO_FIELD(String, sourceIp, "src");
  DTO_FIELD(String, destIp, "dest");
  DTO_FIELD(String, gatewayId, "gatewayId");
  DTO_FIELD(UInt32, sessionId, "sessionId");
  DTO_FIELD(String, redisIp, "redisIp");
  DTO_FIELD(UInt32, redisPort, "redisPort");
};

#include OATPP_CODEGEN_END(DTO)

#endif /* ConfigDto_hpp */