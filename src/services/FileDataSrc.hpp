#ifndef FILE_DATA_SRC_H
#define FILE_DATA_SRC_H

#include "INdrDataSrc.hpp"
#include <string>
#include <mutex>

class FileDataSrc : public INDRDataSrc {
    std::string filename;
    std::mutex mutCoutPacket;
    int pcpap_read();

public:
    FileDataSrc(std::string& filename);
    virtual ~FileDataSrc();
    void StartCapture() override;
    void StopCapture();
};

#endif