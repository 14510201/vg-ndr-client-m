#ifndef INDRDATA_H
#define INDRDATA_H

#include <iostream>
#include <pcapplusplus/PcapFileDevice.h>
#include <pcapplusplus/Packet.h>
#include <pcapplusplus/IPv4Layer.h>
#include "SvcFskModemConnectionUserApi.hpp"

using namespace std;
using namespace svc_fsk_modem;


// Base class INDRData
class INDRDataSrc {

protected:
    uint32_t sessionId;
    std::string srcIp;
    std::string destIp;
    user_api::SvcFskModemConnectionUserApi* pUserConnect;

public:
    virtual ~INDRDataSrc() = default;

    void SetFilters(std::string srcIp, std::string destIp) {
        this->srcIp = srcIp;
        this->destIp = destIp;
    }

    void SetFskHandle(user_api::SvcFskModemConnectionUserApi& userConnect) {
        pUserConnect = &userConnect;
    }

    inline uint32_t GetSessionID(){ return this->sessionId; }

    virtual void StartCapture() = 0;
    virtual void StopCapture() = 0;
    virtual void SetSessionId(uint32_t sessionId) { this->sessionId = sessionId ;}
};

#endif // INDRDATA_H
