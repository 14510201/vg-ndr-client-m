
#include <vector>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include "PspOobPacket.hpp"
#include <iostream>
#include <string>
#include <chrono>
#include <cstdint>
#include <sstream>
#include <vector>
#include <iomanip>
#include <cstring>
#include <thread>
#include <mutex>

using namespace std;


/// <summary>	Constructor. </summary>
///
/// <exception cref="std::runtime_error">	Raised when unable to open the file. </exception>
///
/// <param name="srcIpStr"> 	Source IP, "xxx.xxx.xxx.xxx" format. </param>
/// <param name="destIpStr">	Destination IP, "xxx.xxx.xxx.xxx" format. </param>
PspOobPacket::PspOobPacket( std::string srcIpStr, std::string destIpStr)
{ 

	// parse the IP addresses into uint32s
	vector<string> splitStr;
	boost::split(splitStr, srcIpStr, boost::is_any_of("."));
	srcIp = 0;
	for (string valStr : splitStr)
	{
		srcIp <<= 8;
		srcIp |= (uint8_t)stoi(valStr);
	}
	boost::split(splitStr, destIpStr, boost::is_any_of("."));
	destIp = 0;
	for (string valStr : splitStr)
	{
		destIp <<= 8;
		destIp |= (uint8_t)stoi(valStr);
	}

	// set the protocol
	protocolId = L2TpProtocolId;

	// create a working buffer
	pWorkingBufr = new uint32_t[MaxIqFrameWords];

	// determine if we are on a little-endian machine
	pWorkingBufr[0] = 0x00112233;
	machineIsLittleEndian = (((uint8_t*)pWorkingBufr)[0] == 0x33);

	// general init
	pktLoc = 0;

	// parse the SHB
	ParseSectionHeaderBlock();
}


/// <summary>	Destructor. </summary>
PspOobPacket::~PspOobPacket(void)
{
	// if file is open, close it
	if (pFile)
	{
		fclose(pFile);
		pFile = nullptr;
	}

	// delete buffers
	if (pWorkingBufr) delete pWorkingBufr;
}


/// <summary>	Addition assignment operator. </summary>
///
/// <param name="inc">	Number of matching packets to increment. </param>
PspOobPacket& PspOobPacket::operator +=(const int inc)
{
	SkipPackets(inc);
	return *this;
}


/// <summary>	Function call operator, returns the next packet encoded IQ payload. </summary>
///
/// <returns>	Next packet's encoded IQ payload. </returns>
std::vector<uint32_t> PspOobPacket::operator () (void)
{
	return GetNextPacket();
}

	
/// <summary>	Parse section header block. The SHB is the first block in the file. </summary>
void PspOobPacket::ParseSectionHeaderBlock(void)
{
	uint32_t blockTypeLen[2];
	int remainingBlockBytes;

	// get the block header (type and length)
	if (fread(blockTypeLen, sizeof(uint32_t), 2, pFile) == 2)
	{
		// check type is packet
		if (blockTypeLen[0] == PcapngSectionHeaderBlockType)
		{
			// byte order check
			uint32_t byteOrderMagic;
			fread(&byteOrderMagic, sizeof(uint32_t), 1, pFile);

			// determine if the file is little-endian or big-endian
			fileIsLittleEndian = (byteOrderMagic == PcapngByteOrderMagicLittleEndian);

			// now set remaining bytes now that we know the byte order
			CorrectPcapngFormatEndianness(&blockTypeLen[1]);

			// set remaining block bytes (account for the byte order magic word)
			remainingBlockBytes = blockTypeLen[1] - (3 * sizeof(uint32_t));

			// skip the rest
			fseek(pFile, remainingBlockBytes, SEEK_CUR);

			// success
			return;
		}
	}

	// if we get to here something went wrong
	throw std::runtime_error("Unable to find or parse the SHB. PcapNG file corrupt.");
}


/// <summary>	Skip packets. </summary>
///
/// <param name="count">	Number of matching packets to skip. </param>
void PspOobPacket::SkipPackets(int count)
{
	int remainingCount = count;
	uint32_t blockTypeLen[2];
	int remainingBlockBytes;
	while ((remainingCount != 0) && !feof(pFile))
	{
		// get the block header (type and length)
		if (fread(blockTypeLen, sizeof(uint32_t), 2, pFile) == 2)
		{
			// correct endianness of the blockTypeLen
			CorrectPcapngFormatEndianness(blockTypeLen, 2);

			// set remaining bytes (account for the two uint32s we read)
			remainingBlockBytes = blockTypeLen[1] - (2 * sizeof(uint32_t));

			// check type is packet
			if (blockTypeLen[0] == PcapngPacketBlockType)
			{
				// block header: skip the interface id (32b) and timestamp upper/lower (2*32b)
				fseek(pFile, 3 * sizeof(uint32_t), SEEK_CUR);
				remainingBlockBytes -= 3 * sizeof(uint32_t);

				// block header: next is the captured packet length 
				uint32_t remainingCapturedPktBytes;
				remainingBlockBytes -= fread(&remainingCapturedPktBytes, sizeof(uint32_t), 1, pFile) * sizeof(uint32_t);
				CorrectPcapngFormatEndianness(&remainingCapturedPktBytes);

				// block header: skip the original packet length (32b)
				fseek(pFile, sizeof(uint32_t), SEEK_CUR);
				remainingBlockBytes -= sizeof(uint32_t);

				// we are now at the packet data for the block

				// check for match
				if (FilterMatchPacket(&remainingCapturedPktBytes, &remainingBlockBytes))
				{
					remainingCount -= 1;
				}
			}

			// skip the rest
			fseek(pFile, remainingBlockBytes, SEEK_CUR);
		}
	}
}
int printOneTime = false;
void printHex(const std::vector<uint32_t> &data)
{	
	for (size_t i = 0; i < data.size(); ++i)
	{
		std::cout << "0x";
		std::cout << std::setfill('0') << std::setw(8) << std::hex << data[i] << ' ';			
	}
	std::cout << std::endl;
}

/// <summary>	Gets the next packet. </summary>
///
/// <returns>	Encoded IQ payload of the next packet. </returns>
std::vector<uint32_t> PspOobPacket::GetNextPacket(void)
{
	uint32_t blockTypeLen[2];
	int remainingBlockBytes;
	std::vector<uint32_t> encodedIqDataPayload;

	// get the block header (type and length)
	bool foundMatch = false;
	while (!foundMatch && (fread(blockTypeLen, sizeof(uint32_t), 2, pFile) == 2) && !feof(pFile))
	{
		// correct endianness of the blockTypeLen
		CorrectPcapngFormatEndianness(blockTypeLen, 2);

		// set remaining bytes (account for the two uint32s we read)
		remainingBlockBytes = blockTypeLen[1] - (2 * sizeof(uint32_t));

		// check type is packet
		if (blockTypeLen[0] == PcapngPacketBlockType)
		{
			// block header: skip the interface id (32b) and timestamp upper/lower (2*32b)
			fseek(pFile, 3 * sizeof(uint32_t), SEEK_CUR);
			remainingBlockBytes -= 3 * sizeof(uint32_t);

			// block header: next is the captured packet length 
			uint32_t remainingCapturedPktBytes;
			remainingBlockBytes -= fread(&remainingCapturedPktBytes, sizeof(uint32_t), 1, pFile) * sizeof(uint32_t);
			CorrectPcapngFormatEndianness(&remainingCapturedPktBytes);

			// block header: skip the original packet length (32b)
			fseek(pFile, sizeof(uint32_t), SEEK_CUR);
			remainingBlockBytes -= sizeof(uint32_t);

			// we are now at the packet data for the block

			// check for match
			if (FilterMatchPacket(&remainingCapturedPktBytes, &remainingBlockBytes))
			{
				// get the PSP OOB header word (uint32_t). Note the PSP OOB packet format
				// is big-endien 32b words.
				uint32_t pspOobHeader;
				remainingBlockBytes -= fread(&pspOobHeader, sizeof(uint32_t), 1, pFile) * sizeof(uint32_t);
				CorrectPspOobEndianness(&pspOobHeader);
				remainingCapturedPktBytes -= sizeof(uint32_t);

				// check for seq gap (except first packet loc), then update the last
				uint16_t curSeqNum = pspOobHeader & 0xffff;
				if (pktLoc)
				{
					// gap?
					if (curSeqNum != (lastSeqNumber + 1))
					{
						std::cout << "last processed packet " <<  lastSeqNumber << " present one " << curSeqNum << endl;
						seqGaps.emplace_back(SequenceGap_t(pktLoc, curSeqNum, curSeqNum - lastSeqNumber));
					}
				}
				lastSeqNumber = curSeqNum;
					
				// pull the remaining PSP OOB data
				int remainingCapturedPktWords = remainingCapturedPktBytes / sizeof(uint32_t);
				int pullWordCount = (remainingCapturedPktWords < MaxIqFrameWords) ? remainingCapturedPktWords : MaxIqFrameWords;
				remainingBlockBytes -= fread(pWorkingBufr, sizeof(uint32_t), pullWordCount, pFile) * sizeof(uint32_t);

				// correct the endienness of the buffer (as 32b words)
				CorrectPspOobEndianness(pWorkingBufr, pullWordCount);

				// add it to the vector
				encodedIqDataPayload.insert(encodedIqDataPayload.end(), pWorkingBufr, pWorkingBufr + pullWordCount);

				// found match
				foundMatch = true;

				// update our packet loc
				pktLoc++;
			}
		}

		// skip the rest (if any)
		fseek(pFile, remainingBlockBytes, SEEK_CUR);
	}
	// if(printOneTime == false){
	// 	printHex(encodedIqDataPayload);
	// 	printOneTime = true;
	// }
	// return the iq data
	return encodedIqDataPayload;
}


/// <summary>
/// 	Filter match packet. This will pull the packet headers, through the l2tp header, and
/// 	check for what we want.
/// </summary>
///
/// <param name="pRemainingCapturedPktBytes">	[in,out] Length of the captured packet. </param>
/// <param name="pRemainingBlockBytes">		 	[in,out] Number of remaining bytes in the block,
/// 											updated upon exit. </param>
///
/// <returns>	True if it matches, false if not. </returns>
bool PspOobPacket::FilterMatchPacket(uint32_t* pRemainingCapturedPktBytes, int* pRemainingBlockBytes)
{
	// pull in through the l2tp header so we can ckeck them. If the packet is
	// not long enough to look at the expected headers we know it will not match.
	if (*pRemainingCapturedPktBytes < PacketHeaderBytes)
	{
		return false;
	}

	// get the headers. Update both the remaining block bytes and remaining captured packet bytes
	uint8_t bufr[PacketHeaderBytes];
	*pRemainingBlockBytes -= fread(bufr, sizeof(uint8_t), PacketHeaderBytes, pFile);
	*pRemainingCapturedPktBytes -= PacketHeaderBytes;

	// check protocol type
	if (bufr[HeaderOffsetProtocolType] != protocolId)
	{
		return false;
	}
	
	// check src ip
	uint32_t ipSrcCheck = 0;
	for (int idx = 0; idx < 4; idx++)
	{
		ipSrcCheck <<= 8;
		ipSrcCheck |= bufr[HeaderOffsetSrcIp + idx];
	}
	if (ipSrcCheck != srcIp)
	{
		return false;
	}

	// check src ip 
	uint32_t ipDestCheck = 0;
	for (int idx = 0; idx < 4; idx++)
	{
		ipDestCheck <<= 8;
		ipDestCheck |= bufr[HeaderOffsetDestIp + idx];
	}
	if (ipDestCheck != destIp)
	{
		return false;
	}

	// passed our checks
	return true;
}


/// <summary>
/// 	Correct PcapNG format endianness. This only applies to values of the PcapNG file format,
/// 	not packet data.
/// </summary>
///
/// <param name="pValue">	[in,out] Ptr to the value to swap. </param>
inline void PspOobPacket::CorrectPcapngFormatEndianness(uint32_t* pValue)
{
	// Our machine may be litte or big endian as could the pcapng file.
	// This applies only to the PcapNG file format values, it has nothing
	// to do with packet data.
	// The logic is: 
	//		machineIsLittleEndian = 0, fileIsLittleEndian = 1  SWAP
	//		machineIsLittleEndian = 1, fileIsLittleEndian = 1  NO_SWAP
	//		machineIsLittleEndian = 0, fileIsLittleEndian = 0  NO_SWAP
	//		machineIsLittleEndian = 1, fileIsLittleEndian = 0  SWAP
	if (!(machineIsLittleEndian ^ fileIsLittleEndian)) return;

	// byteswap
	uint8_t* pValueAsBytes = (uint8_t*)pValue;
	*pValue = (pValueAsBytes[0] << 24) | (pValueAsBytes[1] << 16) | (pValueAsBytes[2] << 8) | (pValueAsBytes[3] << 0);
}


/// <summary>
/// 	Correct PcapNG format endianness of a buffer in place. This only applies to values of the
/// 	PcapNG file format, not packet data.
/// </summary>
///
/// <param name="pValues">	[in,out] Ptr to the values to swap. </param>
/// <param name="count">  	Number of words to swap. </param>
inline void PspOobPacket::CorrectPcapngFormatEndianness(uint32_t* pValues, int count)
{
	// Our machine may be litte or big endian as could the pcapng file.
	// This applies only to the PcapNG file format values, it has nothing
	// to do with packet data.
	// The logic is: 
	//		machineIsLittleEndian = 0, fileIsLittleEndian = 1  SWAP
	//		machineIsLittleEndian = 1, fileIsLittleEndian = 1  NO_SWAP
	//		machineIsLittleEndian = 0, fileIsLittleEndian = 0  NO_SWAP
	//		machineIsLittleEndian = 1, fileIsLittleEndian = 0  SWAP
	if (!(machineIsLittleEndian ^ fileIsLittleEndian)) return;

	// byteswap
	uint8_t* pValueAsBytes = (uint8_t*)pValues;
	for (int idx = 0; idx < count; idx++)
	{
		pValues[idx] = (pValueAsBytes[0] << 24) | (pValueAsBytes[1] << 16) | (pValueAsBytes[2] << 8) | (pValueAsBytes[3] << 0);
		pValueAsBytes += 4;
	}
}



/// <summary>	Correct psp oob endianness in place. </summary>
///
/// <param name="pValue">	[in,out] Ptr to the value to swap. </param>
inline void PspOobPacket::CorrectPspOobEndianness(uint32_t* pValue)
{
	// the psp oob format in the network packet is big-endian 32b words. Our
	// machine may be litte or big endian as could the pcapng file itself.
	// The logic is: 
	//		machineIsLittleEndian = 0, fileIsLittleEndian = 1  NO_SWAP
	//		machineIsLittleEndian = 1, fileIsLittleEndian = 1  SWAP
	//		machineIsLittleEndian = 0, fileIsLittleEndian = 0  SWAP
	//		machineIsLittleEndian = 1, fileIsLittleEndian = 0  NO_SWAP
	if (machineIsLittleEndian ^ fileIsLittleEndian) return;

	// byteswap
	uint8_t* pValueAsBytes = (uint8_t*)pValue;
	*pValue = (pValueAsBytes[0] << 24) | (pValueAsBytes[1] << 16) | (pValueAsBytes[2] << 8) | (pValueAsBytes[3] << 0);
}


/// <summary>	Correct psp oob endianness of a buffer in place. </summary>
///
/// <param name="pValue">	[in,out] Ptr to the values to swap. </param>
/// <param name="count"> 	Number of words to swap. </param>
inline void PspOobPacket::CorrectPspOobEndianness(uint32_t* pValues, int count)
{
	// the psp oob format in the network packet is big-endian 32b words. Our
	// machine may be litte or big endian as could the pcapng file itself.
	// The logic is: 
	//		machineIsLittleEndian = 0, fileIsLittleEndian = 1  NO_SWAP
	//		machineIsLittleEndian = 1, fileIsLittleEndian = 1  SWAP
	//		machineIsLittleEndian = 0, fileIsLittleEndian = 0  SWAP
	//		machineIsLittleEndian = 1, fileIsLittleEndian = 0  NO_SWAP
	if (machineIsLittleEndian ^ fileIsLittleEndian) return;

	// byteswap
	uint8_t* pValueAsBytes = (uint8_t*)pValues;
	for (int idx = 0; idx < count; idx++)
	{
		pValues[idx] = (pValueAsBytes[0] << 24) | (pValueAsBytes[1] << 16) | (pValueAsBytes[2] << 8) | (pValueAsBytes[3] << 0);
		pValueAsBytes += 4;
	}
}
