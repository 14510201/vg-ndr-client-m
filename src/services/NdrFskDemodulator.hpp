#ifndef NDR_FSK_DEMODULATOR_H
#define NDR_FSK_DEMODULATOR_H
#include <iostream>
#include <string>
#include <cstring>
#include <chrono>
#include <boost/interprocess/exceptions.hpp>

#include "SvcFskModemConnectionUserApi.hpp"
#include "NdrRedisClient.hpp"
#include "../utils/Logging.hpp"

using namespace svc_fsk_modem;

class NdrFskDemodulator {

private:
    bool running = false;
    bool stop = false;
    std::mutex mutCout;
    log4cpp::Category* logger;

    user_api::SvcFskModemConnectionUserApi *pUserConnect;
    NdrRedisClient *ndrRedisClient;
    void _ServiceAttach();
    void _SetConfiguration(std::string cfgName);
    void _GetActiveChannels();
    void _ShowActiveConfiguration();
    std::string base64_encode(const unsigned char* input, size_t length);
    
public:

    NdrFskDemodulator(user_api::SvcFskModemConnectionUserApi* pUserConnect, NdrRedisClient *ndrRedisClient);
    ~NdrFskDemodulator();
    void _DiagnosticsEnable(bool enable);

    void Initialize();
    void StartDemodPktListenerThread();
    void Stop();

    static std::string CFG_GFSK_50KBPS_ALT;
    static std::string CFG_GFSK_50KBPS;
};

#endif