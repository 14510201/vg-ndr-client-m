#include "FileDataSrc.hpp"
#include <chrono>
#include <iomanip>
#include <iostream>
#include "PspOobPcapng.hpp"
#include <mutex>
#include <boost/date_time/date_generators.hpp>
#include <strstream>
#include <thread>
#include <boost/format/alt_sstream.hpp>
#include <boost/format.hpp>


FileDataSrc::FileDataSrc(std::string &filename)
{
	this->filename = filename;
	cout << "Reading data from file: "<< filename << endl;
}

FileDataSrc::~FileDataSrc()
{
}

void FileDataSrc::StopCapture()
{
}

void printHex(const std::vector<uint8_t> &data)
{
	for (size_t i = 0; i < data.size(); ++i)
	{
		if (i % 4 == 0)
		{
			std::cout << "0x";
		}

		std::cout << std::setfill('0') << std::setw(2) << std::hex << static_cast<int>(data[i]);

		if (i % 4 == 3)
		{
			std::cout << " ";
		}

		if (i % 40 == 39)
		{
			std::cout << std::endl;
		}
	}
	std::cout << std::endl;
}

std::vector<std::string> split(const std::string &str, char delim)
{
	std::vector<std::string> tokens;
	std::stringstream ss(str);
	std::string token;
	while (std::getline(ss, token, delim))
	{
		tokens.push_back(token);
	}
	return tokens;
}
uint32_t ipToInt(const std::string &ip)
{
	auto octets = split(ip, '.');
	if (octets.size() != 4)
		return 0; // Not a valid IPv4 address

	uint32_t ipInt = 0;
	for (int i = 0; i < 4; ++i)
	{
		ipInt |= (std::stoi(octets[i]) << (24 - i * 8));
	}
	return ipInt;
}

std::string getProtocolTypeAsString(pcpp::ProtocolType protocolType)
{
	switch (protocolType)
	{
	case pcpp::Ethernet:
		return "Ethernet";
	case pcpp::IPv4:
		return "IPv4";
	case pcpp::TCP:
		return "TCP";
	case pcpp::HTTPRequest:
	case pcpp::HTTPResponse:
		return "HTTP";
	default:
		return std::to_string(protocolType);
	}
}

int FileDataSrc::pcpap_read()
{
	uint8_t bufr[2048];
	int pktCount;

	PspOobPcapng pcap(this->filename, srcIp, destIp);
	pktCount = 100000;

	// send packets of IQ frames
	for (int i = 0; i < pktCount; i++)
	{
		vector<uint32_t> encodedIqData;

		// get next matching packet
		encodedIqData = pcap();

		// if there was another packet, send
		if (encodedIqData.size())
		{
			// copy into our buffer
			std::copy(encodedIqData.begin(), encodedIqData.end(), (uint32_t *)bufr);

			// if this is the first packet, dump a few encoded IQ 32b words and how they look in
			// the byte buffer we are sending.

			// std::lock_guard<std::mutex> guard(mutCoutPacket);
			// std::cout << endl
			// 		  << "First 4 encoded IQ 32b words: ";
			// string line = "";
			// for (int idx = 0; idx < 4; idx++)
			// {
			// 	line += str(boost::format("0x%|08X|  ") % encodedIqData[idx]);
			// }
			// cout << line << endl;
			// std::cout << "In the byte buffer sent: " << std::hex;
			// line = "";
			// for (int idx = 0; idx < 4 * sizeof(uint32_t); idx++)
			// {
			// 	line += str(boost::format("0x%|02X| ") % (int)bufr[idx]);
			// }
			// cout << line << endl
			// 	 << endl;

			while (pUserConnect->SendDemodIqFrame((void *)bufr, encodedIqData.size() * sizeof(uint32_t)) < 0)
				;
			std::this_thread::sleep_for(std::chrono::microseconds(5));
		}
		else
		{
			break;
		}
	}

	// sequence gaps?
	if (pcap.SequenceGapsDetected())
	{
		auto gaps = pcap.SequenceGaps();
		std::lock_guard<std::mutex> guard(mutCoutPacket);
		std::cout << endl
				  << "GAPS DETECTED! " << std::dec << (int)gaps.size() << " gaps detected." << endl
				  << endl;
	}

	// short delay
	std::this_thread::sleep_for(std::chrono::milliseconds(2));
	return 0;
};

void FileDataSrc::StartCapture()
{
	cout << "Start File Capture Thread\n";
	auto start = std::chrono::high_resolution_clock::now();
	pcpap_read();
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double, std::milli> duration = end - start;
	std::cout << "Function took " << duration.count() << " milliseconds.\n";
};
