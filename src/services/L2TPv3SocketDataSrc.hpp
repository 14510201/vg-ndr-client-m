#ifndef L2TPV3SOCKET_DATA_SRC_H
#define L2TPV3SOCKET_DATA_SRC_H

#include "INdrDataSrc.hpp"
#include "../utils/Logging.hpp"

class L2TPv3SocketDataSrc : public INDRDataSrc {
private:
    std::string interface;
    bool running = false;
    bool stop = false;

public:
    L2TPv3SocketDataSrc(std::string interface);
    ~L2TPv3SocketDataSrc() override;
    void StartCapture() override;
    void StopCapture();

    inline log4cpp::Category& getLogger(){
        return Logging::getInstance().getLogger("L2TPv3SocketDataSrc");
    }
    
    user_api::SvcFskModemConnectionUserApi* GetFskModemApi();
    inline bool ShouldStop();
};

#endif