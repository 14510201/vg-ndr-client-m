#include "L2TPv3SocketDataSrc.hpp"
#include <pcapplusplus/PcapLiveDeviceList.h>
#include <pcapplusplus/SystemUtils.h>

L2TPv3SocketDataSrc::L2TPv3SocketDataSrc(std::string interface)
{
    this->interface = interface;
    log4cpp::Category& logger = log4cpp::Category::getRoot();
    logger.info("Reading data from socket on interface: %s", this->interface.c_str());
}

L2TPv3SocketDataSrc::~L2TPv3SocketDataSrc() {}

inline static void CorrectPspOobEndianness(uint32_t *pValues, int count)
{
    uint8_t *pValueAsBytes = (uint8_t *)pValues;
    for (int idx = 0; idx < count; idx++)
    {
        pValues[idx] = (pValueAsBytes[0] << 24) | (pValueAsBytes[1] << 16) | (pValueAsBytes[2] << 8) | (pValueAsBytes[3] << 0);
        pValueAsBytes += 4;
    }
}

inline bool L2TPv3SocketDataSrc::ShouldStop()
{
    return this->stop;
}

static bool onPacketArrivesBlockingMode(pcpp::RawPacket *packet, pcpp::PcapLiveDevice *dev, void *cookie)
{
    pcpp::Packet parsedPacket(packet);
    L2TPv3SocketDataSrc *datasender = (L2TPv3SocketDataSrc *)cookie;
    const uint8_t *rawData = parsedPacket.getRawPacket()->getRawData();
    pcpp::IPv4Layer *ipLayer = parsedPacket.getLayerOfType<pcpp::IPv4Layer>();
    pcpp::ProtocolType protocolType = ipLayer->getProtocol();
    

    uint8_t *payload = ipLayer->getLayerPayload();
    size_t payloadSize = ipLayer->getLayerPayloadSize();

    uint32_t *payload32 = (uint32_t *)payload;
    int payloadSizeWords = (payloadSize / sizeof(uint32_t));

    CorrectPspOobEndianness(payload32, payloadSizeWords);

    uint32_t _sessionId = payload32[0];
    uint32_t sequenceNumber = payload32[1];
    payload32 += 2;
    payloadSizeWords -= 2;

    if (_sessionId == datasender->GetSessionID())
    {
        if (datasender->GetFskModemApi()->SendDemodIqFrame(payload32, payloadSizeWords * sizeof(uint32_t)) < 0)
        {
        }
    }
    else
    {
        datasender->getLogger().warn("SessionID mismatch: 0x%08x Expected: 0x%08x", _sessionId, datasender->GetSessionID());
    }
    return datasender->ShouldStop();
}

user_api::SvcFskModemConnectionUserApi *L2TPv3SocketDataSrc::GetFskModemApi()
{
    return pUserConnect;
}

void L2TPv3SocketDataSrc::StartCapture()
{
    this->running = true;
    log4cpp::Category& logger = this->getLogger();
    // Read packets from RPD IP and send only LT2P3 packets to the fsksvcmodem
    pcpp::PcapLiveDevice *dev = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByName(this->interface);
    pcpp::PcapLiveDevice::DeviceConfiguration config;
    config.packetBufferTimeoutMs = 1; // set number above 0

    if (dev == NULL)
    {
        logger.error("Cannot find interface: %s", this->interface.c_str());
        StopCapture();
        return;
    }
    if (!dev->open(config))
    {
        logger.error("Cannot open device: %s", this->interface.c_str());
        StopCapture();
        return;
    }
    logger.info("Interface opened: %s (src: %s, dst: %s)", this->interface.c_str(), this->srcIp.c_str(), this->destIp.c_str());
    pcpp::IPFilter ipSrcFilter(srcIp, pcpp::Direction::SRC);
    pcpp::IPFilter ipDestFilter(destIp, pcpp::Direction::DST);
   
    pcpp::AndFilter andFilter;
    andFilter.addFilter(&ipSrcFilter);
    andFilter.addFilter(&ipDestFilter);
   
    dev->setFilter(andFilter);

    logger.info("Interface info:");
    logger.info("   Interface name:        %s", dev->getName().c_str());
    logger.info("   MAC address:           %s", dev->getMacAddress().toString().c_str());
    logger.info("   Default gateway:       %s", dev->getDefaultGateway().toString().c_str());
    logger.info("   Interface MTU:         %d", dev->getMtu());
    dev->startCaptureBlockingMode(onPacketArrivesBlockingMode, this, 0);
}
void L2TPv3SocketDataSrc::StopCapture()
{
    this->stop = true;
}