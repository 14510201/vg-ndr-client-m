#include "INdrDataSrc.hpp"
#include <string>

class NdrDataFactory {
public:
    static INDRDataSrc* CreateFromFile(std::string& filename);
    static INDRDataSrc* CreateFromInterface(std::string& interface);
};