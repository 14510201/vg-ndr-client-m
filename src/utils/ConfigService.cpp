#include "ConfigService.hpp"
#include <iostream>

#include <oatpp/core/utils/String.hpp>

// Initialize static members
ConfigService* ConfigService::instance = nullptr;
std::mutex ConfigService::mutex;

std::string ConfigService::REDIS_IP = "redis-ip";
std::string ConfigService::REDIS_PORT = "redis-port";
std::string ConfigService::GATEWAY_ID = "gateway-id";
std::string ConfigService::FILENAME = "file";
std::string ConfigService::DEST_IP = "dest";
std::string ConfigService::INTERFACE_NAME = "interface";
std::string ConfigService::SESSION_ID = "session-id";
std::string ConfigService::SOURCE_IP = "src";

// Static method for accessing the class instance
ConfigService* ConfigService::getInstance() {
    std::lock_guard<std::mutex> lock(mutex);
    if (instance == nullptr) {
        instance = new ConfigService();
    }
    return instance;
}

// Method for setting a configuration value
void ConfigService::setValue(const std::string& key, const std::variant<uint32_t, std::string>& value) {
    configSettings[key] = value;
}

// Method for getting a configuration value
std::variant<uint32_t, std::string> ConfigService::getValue(const std::string& key) {
    auto it = configSettings.find(key);
    if (it != configSettings.end()) {
        return it->second;
    }
    return ""; // Return empty string if key not found
}

std::string ConfigService::getStrValue(const std::string& key){
    std::variant<uint32_t, std::string> myVariant = getValue(key);
    std::string result = std::visit([](auto&& arg) -> std::string {
        // Use std::to_string for int and direct return for std::string
        if constexpr (std::is_same_v<std::decay_t<decltype(arg)>, std::string>) {
            return arg; // Directly return the string
        } else {
            return std::to_string(arg); // Convert to string for other types
        }
    }, myVariant);
    return result;
}

uint32_t ConfigService::getInt32Value(const std::string& key){
    std::variant<uint32_t, std::string> myVariant = getValue(key);
    uint32_t result = std::get<uint32_t>(myVariant);
    return result;
}

oatpp::Object<ConfigDto> ConfigService::getConfigDto(){

  auto dto = oatpp::Object<ConfigDto>::createShared();
  dto->interface = getStrValue(ConfigService::INTERFACE_NAME);
  dto->sourceIp = getStrValue(ConfigService::SOURCE_IP);
  dto->destIp = getStrValue(ConfigService::DEST_IP);
  dto->gatewayId = getStrValue(ConfigService::GATEWAY_ID);
  dto->sessionId = getInt32Value(ConfigService::SESSION_ID);
  dto->redisIp = getStrValue(ConfigService::REDIS_IP);
  dto->redisPort = getInt32Value(ConfigService::REDIS_PORT);

  return dto;

} 
