#ifndef CONFIG_SERVICE_H
#define CONFIG_SERVICE_H

#include <string>
#include <map>
#include <mutex>
#include <variant>
#include <variant>
#include "../dto/ConfigDto.hpp"

class ConfigService {
    
private:
    // Private static instance of the class
    static ConfigService* instance;

    // Private constructor for preventing external instantiation
    ConfigService() {}

    // Container for storing configuration settings
    std::map<std::string, std::variant<uint32_t, std::string>> configSettings;

    // Mutex for thread-safe instance creation
    static std::mutex mutex;

public:
    // Delete copy constructor and assignment operator
    ConfigService(const ConfigService&) = delete;
    ConfigService& operator=(const ConfigService&) = delete;

    // Static method for accessing the class instance
    static ConfigService* getInstance();

    // Method for setting a configuration value (overloaded for int and std::string)
    void setValue(const std::string& key, const std::variant<uint32_t, std::string>& value);
    std::string getStrValue(const std::string& key);
    uint32_t getInt32Value(const std::string& key);
    // Method for getting a configuration value
    std::variant<uint32_t, std::string> getValue(const std::string& key);

    oatpp::Object<ConfigDto> getConfigDto();

    static std::string REDIS_IP;
    static std::string REDIS_PORT;
    static std::string SESSION_ID;
    static std::string GATEWAY_ID;
    static std::string FILENAME;
    static std::string RPD_IP;
    static std::string SOURCE_IP;
    static std::string DEST_IP;
    static std::string INTERFACE_NAME;

};

#endif // CONFIG_SERVICE_H
