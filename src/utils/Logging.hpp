#ifndef LOGGING_HPP
#define LOGGING_HPP
#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>

class Logging {

public:
    static Logging& getInstance() {
        static Logging instance;
        return instance;
    }

    Logging(const Logging&) = delete;
    void operator=(const Logging&) = delete;
    log4cpp::Category& getLogger(std::string category);

private:
    Logging();
    ~Logging();
    // Your existing members/functions here...
};
#endif