#include <iostream>
#include <string>
#include <cstring>
#include <chrono>
#include <boost/interprocess/exceptions.hpp>
#include <boost/format.hpp>
#include <signal.h>

#include <boost/program_options.hpp>
#include "services/INdrDataSrc.hpp"
#include "NdrClient.hpp"
#include "services/NdrDataFactory.hpp"
#include "utils/Logging.hpp"
#include "oatpp/web/server/HttpConnectionHandler.hpp"
#include "oatpp/network/Server.hpp"
#include "oatpp/network/tcp/server/ConnectionProvider.hpp"
#include "oatpp/parser/json/mapping/ObjectMapper.hpp"
#include "oatpp-swagger/Controller.hpp"
#include "components/AppComponent.hpp"
#include "controller/StaticController.hpp"
#include "controller/UserController.hpp"
#include "controller/ConfigController.hpp"

using namespace std;

namespace po = boost::program_options;

NDRClient *ndrClient = nullptr;

// void SigTermHandler(int signo, siginfo_t *info, void *context)
// {
//     if (signo == SIGTERM)
//     {
//         if (ndrClient)
//         {
//             ndrClient->Shutdown();
//         }
//     }
// }

void SigTermHandler(int signum)
{
    log4cpp::Category& logger = Logging::getInstance().getLogger("NDRClient");
    logger.info("Interrupt signal (" + std::to_string(signum) + ") received.");
    if (ndrClient != nullptr)
    {
        ndrClient->Shutdown();
    }
}

int main(int argc, char *argv[])
{
    log4cpp::Category& logger = Logging::getInstance().getLogger("NDRClient");
    try
    {
        po::options_description desc("Options");
        desc.add_options()("help,h", "produce help message")
        ("version,v", "print version string")
        ("file,f", po::value<std::string>(), "pcapng file to read")
        ("session-id,S", po::value<std::string>(), "Session ID to capture")
        ("gateway-id,g", po::value<std::string>(), "Gateway ID")
        ("interface,i", po::value<std::string>(), "Ethernet Interface to capture")
        ("src,s", po::value<std::string>(), "Source IP address")
        ("dest,d", po::value<std::string>(), "Destination IP address")
        ("redis-ip,r", po::value<std::string>(), "Redis server address")
        ("redis-port,p", po::value<std::string>(), "Redis server port")
        ("redis-pwd,w", po::value<std::string>(), "Redis server password");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help"))
        {
            std::cout << desc << "\n";
            return 1;
        }

        if (vm.count("version"))
        {
            std::cout << "Version 1.0.0\n";
        }
        if (vm.count("interface") > 0 && vm.count("file") > 0)
        {
            std::cout << "Please specify either interface or file, but not both" << endl;
            std::cout << desc << "\n";
            return 1;
        }
        if (!(vm.count(ConfigService::REDIS_IP) && 
            vm.count(ConfigService::REDIS_PORT) && 
            vm.count(ConfigService::GATEWAY_ID) &&
            vm.count(ConfigService::SESSION_ID) &&
            vm.count(ConfigService::SOURCE_IP) &&
            vm.count(ConfigService::DEST_IP)
            ))
        {
            std::cout << "Options redis-ip, redis-port, gateway-id, session-id, src and dest are required\n";
            std::cout << desc << "\n";
            return 1;
        }

        ConfigService *config = ConfigService::getInstance();
        std::string sessionId = vm["session-id"].as<std::string>();

        if (sessionId.find("0x") == 0 || sessionId.find("0X") == 0)
        {
            sessionId.erase(0, 2); // Remove the '0x' prefix
        }
        uint32_t sessionIdLong = 0;
        try
        {
            sessionIdLong = std::stoul(sessionId, nullptr, 16);
            config->setValue(ConfigService::SESSION_ID, sessionIdLong);
            logger.debug("The session ID is: %d (0x%08x)", sessionIdLong, sessionIdLong);
        }
        catch (const std::invalid_argument &ia)
        {
            std::cerr << "Invalid session ID: " << ia.what() << std::endl;
            std::cout << desc << "\n";
            return 1;
        }
        catch (const std::out_of_range &oor)
        {
            std::cerr << "Session ID of Range error: " << oor.what() << std::endl;
            std::cout << desc << "\n";
            return 1;
        }
       
        std::string redisIp = vm[ConfigService::REDIS_IP].as<std::string>();
        std::string portStr = vm[ConfigService::REDIS_PORT].as<std::string>();
        std::string gateway = vm[ConfigService::GATEWAY_ID].as<std::string>();
        std::string session = vm[ConfigService::SESSION_ID].as<std::string>();
        std::string src = vm[ConfigService::SOURCE_IP].as<std::string>();
        std::string dest = vm[ConfigService::DEST_IP].as<std::string>();

        uint32_t redisPort = std::stoul(portStr);

        config->setValue(ConfigService::REDIS_IP, redisIp);
        config->setValue(ConfigService::GATEWAY_ID, gateway);
        config->setValue(ConfigService::REDIS_PORT, redisPort);
        config->setValue(ConfigService::SOURCE_IP, src);
        config->setValue(ConfigService::DEST_IP, dest);

        // struct sigaction sigAct = {0};
        // sigAct.sa_flags = SA_SIGINFO;
        // sigAct.sa_sigaction = &SigTermHandler;
        // if (sigaction(SIGTERM, &sigAct, NULL) == -1)
        // {
        //     std::cout << "Unable to install SIGTERM handler" << endl
        //               << flush;
        //     exit(EXIT_FAILURE);
        // }
        signal(SIGINT, SigTermHandler);
        INDRDataSrc *ndrDataSrc = NULL;

        if (vm.count("file"))
        {
            std::string filename = vm["file"].as<std::string>();
            config->setValue(ConfigService::FILENAME,filename);
            ndrDataSrc = NdrDataFactory::CreateFromFile(filename);
        }
        else if (vm.count("interface"))
        {
            std::string interface = vm["interface"].as<std::string>();
            config->setValue(ConfigService::INTERFACE_NAME, interface);
            ndrDataSrc = NdrDataFactory::CreateFromInterface(interface);
        }
        else
        {
            std::cout << "Please specify either interface or file" << endl;
            std::cout << desc << "\n";
            return 1;
        }
        ndrClient = new NDRClient(ndrDataSrc);
        ndrClient->Initialize();
        ndrClient->Start();
    }

    catch (const po::error &ex)
    {
        std::cerr << ex.what() << '\n';
        return 1;
    }
}


NDRClient::NDRClient(INDRDataSrc * ndrDataSrc){
    this->config = ConfigService::getInstance();
    this->ndrDataSrc = ndrDataSrc;
}

NDRClient::~NDRClient(){
    delete this->redisClient;
    delete this->fskDemodulator;
    delete this->ndrDataSrc;
}

void NDRClient::Initialize(){
    std::string redisIp = this->config->getStrValue(ConfigService::REDIS_IP);
    uint32_t redisPort = this->config->getInt32Value(ConfigService::REDIS_PORT);
    std::string gateway = this->config->getStrValue(ConfigService::GATEWAY_ID);
    std::string srcIp = this->config->getStrValue(ConfigService::SOURCE_IP);
    std::string destIp = this->config->getStrValue(ConfigService::DEST_IP);

    this->userConnect = user_api::SvcFskModemConnectionUserApi();
    uint32_t sessionId = this->config->getInt32Value(ConfigService::SESSION_ID);

    this->ndrDataSrc->SetFilters(srcIp, destIp);
    this->ndrDataSrc->SetSessionId(sessionId);
    this->ndrDataSrc->SetFskHandle(this->userConnect);

    this->redisClient = new NdrRedisClient(redisIp, redisPort, "ndr_" + gateway);
    this->fskDemodulator = new NdrFskDemodulator(&userConnect, this->redisClient);    
    this->fskDemodulator->Initialize();
}

void NDRClient::run() {
  AppComponent components; // Create scope Environment components
  
  /* Get router component */
  OATPP_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>, router);

  oatpp::web::server::api::Endpoints docEndpoints;

  docEndpoints.append(router->addController(UserController::createShared())->getEndpoints());

  docEndpoints.append(router->addController(ConfigController::createShared())->getEndpoints());

  router->addController(oatpp::swagger::Controller::createShared(docEndpoints));
  router->addController(StaticController::createShared());

  /* Get connection handler component */
  OATPP_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>, connectionHandler);

  /* Get connection provider component */
  OATPP_COMPONENT(std::shared_ptr<oatpp::network::ServerConnectionProvider>, connectionProvider);

  /* create server */
  oatpp::network::Server server(connectionProvider,
                                connectionHandler);
  
  OATPP_LOGD("Server", "Running on port %s...", connectionProvider->getProperty("port").toString()->c_str());
  
  server.run();

  /* stop db connection pool */
  OATPP_COMPONENT(std::shared_ptr<oatpp::provider::Provider<oatpp::sqlite::Connection>>, dbConnectionProvider);
  dbConnectionProvider->stop();
}

void NDRClient::Start(){
    if(!running || !stop) {
        running = true;
        stop = false;

        std::thread fskDemodulatorThread(&NdrFskDemodulator::StartDemodPktListenerThread, this->fskDemodulator);
        std::thread dataSrcThread(&INDRDataSrc::StartCapture, this->ndrDataSrc);

        oatpp::base::Environment::init();
        this->run();
        oatpp::base::Environment::destroy();

        dataSrcThread.join();
        fskDemodulatorThread.join();
        running = false;
    } else {
        cout << "Failed to start NDRClient";
    }
}

void NDRClient::Shutdown(){
    log4cpp::Category& logger = Logging::getInstance().getLogger("NDRClient");
    logger.info("Shutting down NDRClient");
    this->stop = true;
    this->ndrDataSrc->StopCapture();
    this->fskDemodulator->Stop();
}
